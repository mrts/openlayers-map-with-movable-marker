import 'ol/ol.css';
import Feature from 'ol/Feature';
import Map from 'ol/Map';
import View from 'ol/View';
import Point from 'ol/geom/Point';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import {OSM, Vector as VectorSource} from 'ol/source';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style';
import {fromLonLat} from 'ol/proj';

var initialCoordinates = [24.71704508, 59.40132153];

var view = new View({
  center: fromLonLat(initialCoordinates),
  zoom: 21
});

var map = new Map({
  layers: [
    new TileLayer({
      source: new OSM()
    })
  ],
  target: 'map',
  view: view
});

var positionFeature = new Feature();
positionFeature.setStyle(new Style({
  image: new CircleStyle({
    radius: 6,
    fill: new Fill({
      color: '#3399CC'
    }),
    stroke: new Stroke({
      color: '#fff',
      width: 2
    })
  })
}));

window.updateLocation = function (lon, lat, floor) {
    const coordinates = fromLonLat([lon, lat]);
    positionFeature.setGeometry(new Point(coordinates));
}

window.updateLocation(...initialCoordinates);

new VectorLayer({
  map: map,
  source: new VectorSource({
    features: [positionFeature]
  })
});