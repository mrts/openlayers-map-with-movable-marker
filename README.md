# OpenLayers map with movable marker

Based on [OpenLayers example](https://openlayers.org/en/latest/examples/geolocation.html).

Available at [https://mrts.gitlab.io/openlayers-map-with-movable-marker/](https://mrts.gitlab.io/openlayers-map-with-movable-marker/).

Build:

    npm install
    npm run build
